class AddDefaultValueToAskedInQuestions < ActiveRecord::Migration
  def up
    change_column :questions, :asked, :boolean, default: false
  end
  
  def down
    change_column :questions, :asked, :boolean, default: nil
  end
  
end
