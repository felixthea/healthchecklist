class RemoveIllnessColumnFromQuestions < ActiveRecord::Migration
  def up
    remove_column :questions, :illness
  end

  def down
    add_column :questions, :illness, :string
  end
end
