class IllnessesController < ApplicationController
  
  before_filter :get_illness
  before_filter :get_illnesses
  
  def get_illness
    @illness = Illness.find_by_id(params[:id])
  end
  
  def get_illnesses
    @illnesses = Illness.all
  end
  
  def index
    # @illnesses = Illness.all # made unnecessary with the before_filter :get_illnesses call
    # @illness = Illness.find_by_id(params[:id]) # made unnecessary with the before_filter :get_illness call
    @illness = Illness.new # to allow user to create an illness from the illness index
  end
  
  def show
    #@illness  = Illness.find_by_id(params[:id]) # made unnecessary with the before_filter :get_illness call
  end
  
  def new
    @illness = Illness.new
  end
    
  def create
    @illness = Illness.create(params[:illness])
    redirect_to illnesses_path if @illness.save
  end
  
  def destroy
    @illness.destroy
    redirect_to illnesses_path
  end
 
end
