class QuestionsController < ApplicationController
  
  before_filter :get_illness
  
  def get_illness
    @illness = Illness.find(params[:illness_id]) #illness_id because that's the name of the attribute in the questions model
  end
  
  def index
    @questions = @illness.questions
    # @question = @illness.questions.create(params[:question])
  end
  
  def show
    @question = @illness.questions.find(params[:id]) #id here is the id of the question since we're in the question controller
  end
  
  def new
    @question = @illness.questions.new
  end
  
  def create
    @question = @illness.questions.create(params[:question])
  end
  
  def destroy
    @question = Question.find(params[:id])
    @question.destroy
    redirect_to illness_questions_path
  end

end
