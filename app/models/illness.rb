class Illness < ActiveRecord::Base
  attr_accessible :illness
  has_many :questions
end
