class Question < ActiveRecord::Base
  attr_accessible :asked, :question
  belongs_to :illness
end
